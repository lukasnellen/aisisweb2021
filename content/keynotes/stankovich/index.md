---
#linkTitle:
title: "Regulating AI and Big Data Deployment in Healthcare: Proposing Robust and Sustainable Solutions for Developing Countries' Governments"
speaker: "Mirjana Stankovich"
date: 2021-09-30T16:32:18-05:00
draft: false
weight: 53
---

Delivered by Bratislav Stankovic, multidisciplinary professor of law & biotechnology, and a registered U.S. patent attorney, working on law, science and policy related to innovation, patents, technology transfer, bioethics, research ethics, reproductive technologies, stem cells, and plant space biology.

### Time

{{< time "2021-10-13T13:30:00Z" 0 45 "Keynote 3" >}}

{{< video src="keynote-3-stankovich.mp4" >}}


{{% abstract %}}
AI deployment in healthcare potentially drives game-changing improvements for underserved communities and developing countries in general. From enabling community-health workers to better serve patients in remote rural areas to helping governments in developing countries prevent deadly disease outbreaks, there is growing recognition of the potential of AI tools to improve health access, quality, and cost.

However, the deployment of AI in resource-constrained settings has been surrounded by a lot of hype; and more research is needed on how to deploy best and effectively scale AI solutions in health systems across developing countries. It is challenging to take disruptive technology innovations from developed countries and replicate them to address the unique needs of the developing world.

The speech will identify both barriers to AI deployment at scale in developing countries and the types of regulatory and public policy actions that can best accelerate the appropriate use of AI to improve healthcare in developing countries' contexts. While AI technologies hold great potential for improving healthcare around the globe, these technologies cannot be considered a panacea for solving global health challenges. Adoption, acceleration, and use of AI should strengthen local health systems and be owned and driven by the needs and priorities of developing countries' governments and stakeholders to help them best serve their populations.
{{%/ abstract %}}

{{% bio %}}
Dr. Miriam Stankovich is a Senior Digital Policy Specialist with the Center for Digital Acceleration, DAI. She has rich research experience in leading complex projects in data protection and governance, intellectual property and technology law, regulatory impact assessment of emerging technologies (AI, Blockchain, IoT, Virtual/Augmented Reality, and 3D Printing), and technology transfer. She has also served as a senior legal and policy advisor for major international organizations such as the International Telecommunication Union, the World Bank, World Intellectual Property Organization, the International Finance Corporation, United Nations Industrial Development Organization, United Nations Interregional Crime and Justice Research Institute, United Nations Development Programme, the Inter-American Development Bank, the African Development Bank, African Union, and the European Union.

She was a Fulbright Scholar in intellectual property and technology law at Duke University School of Law and a World Bank McNamara Scholar in development economics, intellectual property rights, and tech transfer at Duke University School of Public Policy. She is the author of numerous peer-reviewed articles, books, monographs, book chapters, laws, and government reports in international intellectual property, technology transfer, innovation, public policy, and regulation of emerging technologies.
{{%/ bio %}}
