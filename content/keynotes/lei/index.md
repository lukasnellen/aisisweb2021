---
#linkTitle:
title: "Planetary Computer for Biodiversity"
speaker: "Bonnie Lei"
date: 2021-09-28T18:46:00-05:00
draft: false
weight: 52
---


### Time

{{< time "2021-10-12T14:15:00Z" 0 45 "Keynote 2" >}}

{{< video src="keynote-2-lei.mp4" >}}

{{% abstract %}}
Nature and the benefits that it provides to people are the foundation of our global economy, our culture, and the overall human experience.  We depend on clean air, water, food, medicine, energy, and building materials that nature provides, but these very ecosystems are threatened or already in decline. Maintaining nature for the benefit of current and future generations is one of humanity’s greatest challenges. The Planetary Computer combines a multi-petabyte catalog of global environmental data with intuitive APIs, a flexible scientific environment that allows users to answer global questions about that data, and applications that put those answers in the hands of conservation stakeholders.
{{%/ abstract %}}

{{% bio %}}
Bonnie Lei is a founding member and Head of Global Strategic Partnerships for Microsoft’s AI for Earth program, a five-year, $50 million cross-company effort delivering technology-enabled solutions to global environmental challenges. In this role, she has awarded 750+ grants in 100+ countries, is helping build the first Planetary Computer, and leads Microsoft’s effort to protect more land than it uses by 2025. Previously, she helped initiate the marine program for Wildlife Conservation Society in Myanmar, discovered a new sea slug species in the Caribbean, and researched climate adaptation of endangered penguins in South Africa. She received her degree in Organismic and Evolutionary Biology at Harvard University, and was an inaugural Schwarzman Scholar at Tsinghua University.
{{%/ bio %}}
