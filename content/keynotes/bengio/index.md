---
#linkTitle:
title: "AI for Scientific Discovery"
speaker: "Yoshua Bengio"
date: 2021-09-28T22:41:18-05:00
draft: false
weight: 54
---
### Time

{{< time "2021-10-14T13:30:00Z" 0 45 "Keynote 4" >}}

{{< video src="keynote-4-bengio.mp4" >}}

{{% abstract %}}
How can machine learning algorithms help scientists make new discoveries, build models, design experiments, design new drugs or new materials? This presentation will highlight a few relevant ideas from the recent literature, including that of generative active learning, of black-box exploration, and how machine learning could come to the rescue when Monte-Carlo Markov chain methods fail but there is underlying structure that can be exploited and lead to generalization.
{{%/ abstract %}}

{{% bio %}}
Yoshua Bengio is Full Professor in the Department of Computer Science and Operations Research at Université de Montreal, as well as the Founder and Scientific Director of Mila and the Scientific Director of IVADO. Considered one of the world’s leaders in artificial intelligence and deep learning, he is the recipient of the 2018 A.M. Turing Award with Geoff Hinton and Yann LeCun, known as the Nobel prize of computing. He is a Fellow of both the Royal Society of London and Canada, an Officer of the Order of Canada, and a Canada CIFAR AI Chair.
{{%/ bio %}}
