---
#linkTitle:
title: "Why should we care about quantum technologies ?"
speaker: "Neil Abroug"
date: 2021-10-08T01:02:46-05:00
draft: false
weight: 55
---
### Time

{{< time "2021-10-15T12:00:00" 0 45 "Keynote 5" >}}

{{< video src="keynote-5-abroug.mp4" >}}

{{% abstract %}}
Governments and big techs are investing billions of dollars in R&D to develop quantum systems all around the world. Even if quantum technologies still faces a big amount of unanswered questions, not only on technological feasibility, but also at the level of basic science. What is the rationale behind such investments ? Is there any over-hype on quantum technologies ? What are the expected rewards of these investments ?
{{% /abstract %}}

{{% bio %}}
Neil Abroug coordinates the French national quantum strategy having a 1.8 Billion EUR investment portfolio. He began his career as a research engineer and project manager at CEA in the field of Industry 4.0. In 2018, he joined the Directorate General for Enterprises at the French Ministry of Economic Affairs where he contributed to setting up the national quantum agenda. In particular, he was the rapporteur of the parliamentary report “Quantum: the technological shift that France will not miss”. Neil is a Dr. Engineer in applied mathematics. He also graduated in Innovation Management and Competitive Intelligence.
{{% /bio %}}
