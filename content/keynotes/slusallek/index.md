---
#linkTitle:
title: "Digital Reality: Understanding the World with AI"
speaker: "Philipp Slusallek"
date: 2021-10-14T07:32:38-05:00
draft: false
weight: 56
---
### Time

{{< time "2021-10-15T14:30:00" 0 45 "Keynote 6" >}}

{{< video src="keynote-6-slusallek.mp4" >}}

[Slides](AISIS-Keynote-Oct-2021.pdf)

{{% abstract %}}
Humans understand and interact with the world around them through iteratively creating and refining useful models of reality. How can we do the same for AI? Learning digital models of reality, automatically exploring those models to identify configurations that can teach us new things, generating synthetic data through simulations for training, benchmarking, validation, and eventually certification of AI systems, and finally closing the loop by identifying where our current models for understanding the world need to be improved to better match reality.
{{% /abstract %}}

{{% bio %}}
Philipp Slusallek is scientific director and member of the executive board at the German Research Center for Artificial Intelligence (DFKI), where he heads the research area on Agents and Simulated Reality. Prof. Slusallek co-founded and is director of strategy at the European AI initiative CLAIRE (Confederation of Laboratories for Artificial Intelligence Research in Europe, claire-ai.org) since 2018. At Saarland University he has been a professor for computer graphics since 1999, a principle investigator at the German Excellence-Cluster on “Multimodal Computing and Interaction” from 2007 to 2019, and co-founder and director for research at the Intel Visual Computing Institute 2009-2017. Before coming to Saarland University, he was a visiting assistant professor at Stanford University. He is a member of acatech (German National Academy of Science and Engineering), a fellow of Eurographics, and was a member of the High-Level Expert Group on Artificial Intelligence for the European Commission and an associate editor for Computer Graphics Forum.

He originally studied physics in Frankfurt and Tübingen (Diploma/MSc) and got his PhD in computer science from Erlangen University. His research covers a wide range of topics including artificial intelligence in a broad sense, simulated/digital reality, real-time and realistic graphics, high-performance computing and simulation, motion synthesis, novel programming models for CPU/GPU/FPGA, computational science, and others.
{{% /bio %}}
