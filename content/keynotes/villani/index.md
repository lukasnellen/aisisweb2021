---
#linkTitle:
title: "AI & Quantum computing — new tools to adapt to our goals"
speaker: "Cédric Villani"
date: 2021-10-09T00:07:41-05:00
draft: false
weight: 57
---
### Time

{{< time "2021-10-11T16:15:00Z" 0 45 "Keynote 7" >}}

{{< video src="villani-keynote-7-aisis-2021.mp4" >}}

{{% abstract %}}
AI and quantum technologies are two ongoing waves of innovation in information treatment. Both have known winters and summers. AI is about automating some specialized, well-defined tasks, already works in many cases, and has to be developed through interdisciplinary experiments. Quantum computing is about extremely fast computing of some algorithms and is not yet working, rather at research stage. AI is everybody’s tasks currently, while quantum technologies are just for specialists. None of these tools will be a panacea, but they can be part of the toolbox for society, provided that we use them in a responsible way, and that they do not distract us from choosing the pillars for our future.
{{% /abstract %}}

{{% bio %}}
Cédric Villani
is a French mathematician. He has received numerous awards for his work including the Fields Medal in 2010, often described as the Nobel Prize in Mathematics.
In 2017, he was elected Member of Parliament for the 5th district of Essonne.  As part of this mandate he led an initiative on the implementation of a French and European strategy in Artificial Intelligence. He also chairs the Parliamentary Office for the Evaluation of Scientific and Technological Choices, which studies all matters in which an in-depth scientific study is mandatory for political action. He is a member of the French Academy of Sciences, as well as of the Pontifical Academy of Sciences.
{{% /bio %}}
