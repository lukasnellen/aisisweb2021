---
#linkTitle:
title: "Simulation Digital Twins and Hybrid AI: enabling 360° prescriptive analytics"
speaker: Michel Morvan
date: 2021-09-30T16:37:40-05:00
draft: false
weight: 51
---
### Time

{{< time "2021-10-11T13:45:00Z" 0 45 "Keynote 1" >}}

{{< video src="keynote-1-morvan.mp4" >}}

{{% abstract %}}
The greater the level of uncertainty, the more likely that decision makers will lose visibility and understanding of the impacts that disruptions and decisions will have on their organization. In this keynote, Michel Morvan explains how simulation digital twins with an hybrid AI approach can help decision-makers to project the future behavior of their organization, detecting emerging behaviors, predicting results and prescribing actions even under conditions that have never occurred before. Michel Morvan is co-founder and Executive Chairman of Cosmo Tech, and a member of the OECD Network of Experts on Artificial Intelligence.
{{%/ abstract %}}

{{% bio "Co-founder and Executive Chairman of Cosmo Tech" %}}
Prior to co-founding Cosmo Tech, Michel was Chief Scientist & Corporate Vice President for Strategic Intelligence and Innovation at Veolia Environment.

Michel is a former Full Professor of Computer Science at École Normale Supérieure in Lyon, former Chair of Complex Systems Modeling & Senior Scientist at the École des Hautes Etudes en Sciences Sociales in Paris, and former External Professor at the Santa Fe Institute in New Mexico (USA). He is an Eisenhower Fellow, a member of the OECD Network of Experts on Artificial Intelligence and the President of the Institute for Technological Research SystemX, boosting the digital transformation of French industry.
{{%/ bio %}}
