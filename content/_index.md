---
pre_title: Welcome to the Second Symposium on
title: "Artificial Intelligence for Science, Industry and Society"
date: 2021-05-07T10:01:21-05:00
draft: false
---

The second edition of AISIS will be hosted **online** by the National Autonomous University of Mexico (UNAM), the largest university in Latin America, from the 11th to 15th of October 2021. *Only [registered participants](contributions/) will have access to the event and to some part of the material presented.*

The AISIS conference series was initiated in October 2019 and held in Mexico City. [AISIS 2019](http://epistemia.nucleares.unam.mx/web?name=AISIS2019) featured 8 keynote speakers and 65 total presentations. The conference brought together 185 scientists, industry representatives, and policy makers from around the world, discussing the implementation of AI in the disciplines of astronomy and astrophysics, facial recognition, medicine, particle and nuclear physics, pharmacy, and many others. We also discussed specific ethical- and regulatory-related aspects, which are important in order to ensure the long-term future of the field, including sustainability, ethics, transparency, and human control over the technology.

The world in 2021 is quite a different place than it was in 2019. More than ever, Artificial Intelligence and Machine Learning have permeated all parts of modern life. Notably, AI has facilitated the global response to the COVID-19 pandemic, helping to understand the virus, accelerating medical research into drugs and treatments, and assisting public health measures through better prediction of virus spread and contact tracing. AI may prove to be a game changer for many societal challenges, including climate change mitigation and adaptation, autonomous and sustainable mobility, the preservation of biodiversity, environmental pollution control, and more.

It is thus important to review the latest advances, both hardware- and methodology-related, trying, as much as possible, to bring the various applications of AI, and their consequences on society, together into a single meeting. New techniques are particularly powerful when dealing with unstructured data or data with complex, non-linear relationships, which are hard to model and analyze with traditional, statistical tools. This has triggered a flurry of activities, both in industry and science, aimed at developing methods to tackle problems which used to be either impossible or at least extremely hard to deal with.

We want to reflect on the richness of the applications of AI, as well as the importance of the computing industry in facilitating the access to the latest developments, to the largest number of people from a wide variety of backgrounds.

We look forward to discussing recent developments in, applications for, and societal implications of Artificial Intelligence with you in October 2021!

<p><img src="banner_aisis2021_800x450.jpg" style="width: 100%;" /></p>

[contributions]: {{< ref "contributions.md" >}}
