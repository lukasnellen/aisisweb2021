---
title: "AISIS Prizes"
date: 2021-05-11T10:48:54-05:00
draft: false
weight: 30
---
An AISIS prize will be awarded for each of the pillars: Science, Industry and Society, based on the video contributions visible in the [Sessions and Contributions section](../sessions/), evaluated by the full symposium programme committee, according to our evaluation criteria as follows:

- Is AI being used in an interesting and/or innovative way to solve the task of interest?

- Is the contribution confined to a very specific topic of interest, or is it more generally of relevance to the community? We would like to promote interdisciplinary activities.

- Is the contribution relevant to only one of the AISIS pillars (science, industry, and society), or is it transversal and thus of importance for the full community?

- Is the presentation accessible? Can people from other sessions understand both what is being done, and the importance of what is being done?

The winners will be awarded with a plenary talk in the final [Prizes session](../sessions/session10/) of the symposium to convey their ideas to the symposium, and will receive a certificate of recognition for their excellent contribution.
