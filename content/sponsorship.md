---
title: "Sponsorship"
date: 2021-05-10T11:29:01-05:00
draft: true
weight: 60
---
## Gold sponsors

The gold sponsors (9000USD ) will have the right to participate in the election of the best contribution, will have their logos on the confreence poster and will have the possibility to advetise their services; they will receive a special diploma and medal.


## Silver sponsors

The silvers sponsors (4000 USD) will their logo displayed on the poster and a special diploma.


## Contact
Those interested should contact Dr. Guy Paic (guy.paic@cern.ch)

## FAQ

  - Will the press be attending the event?

    Yes we plan to have the press covering the event. Of course the more interesting conributions, the more coverage. If the industry can time some new feature at the time of the conference it would be excellent.

  - Will the event be recorded, live-streamed, or further distributed?

    The event will be on-line but limited to registered participants. There will be no other real-time distribution of the event.

  - Will the event be on the record?

    The organizers offer participants the possiblity to upload the presentation and possible accompanying material.

  - Will the event be open to public?

    The event will be open for anyone to register to attend, although it is expected that it will be primarily composed of academics and industry leaders from various fields relating to the topic of the symposium, "Artificial Intelligence for Science, Industry, and Society".

  - Will attendees be under NDA?

    No, the attendees will not be under any such agreements.

  - Will there be Q&A?

    This was not foreseen, rather we are aiming to provide a forum mixing together academic and industry talks for mutual inspiration and to potentially learn from what other fields are doing. Topical round table session will provide room for discussions.

  - Will investors or securities professionals be present at the event?

    As the event will have an open registration, this is possible, but it is neither planned nor expected.  The intended audience of the symposium is primarily research-based, either in the public or private sectors, rather than specific applications such as finance or otherwise.
