---
linkTitle: Programme
title: "Scientific Programme and Timetable"
date: 2021-05-08T22:27:32-05:00
draft: false
weight: 10
---
## Keynote speakers

- Yoshua Bengio, Scientific Director, [MILA](https://mila.quebec/en/)
- Bonnie Lei, Head of Global Strategic Partnerships - [AI for Earth at Microsoft](https://www.microsoft.com/en-us/ai/ai-for-earth)
- Mirjana Stankovich, Senior Digital Policy Specialist, [Center for Digital Acceleration, DAI](https://www.dai.com/)
- Neil Abroug, [National Coordinator of the Quantum strategy](https://www.gouvernement.fr/en/quantum-plan), [General Secretariat for Investment](https://www.gouvernement.fr/le-secretariat-general-pour-l-investissement), Prime Minister’s Office, France
- Michel Morvan, co-founder and Executive Chairman of [Cosmo Tech](https://cosmotech.com/)
- Prof. Dr.-Ing. Philipp Slusallek, Executive Director, [German Research Centre for Artificial Intelligence (DFKI) Saarbrücken](https://www.dfki.de/web/ueber-uns/standorte-kontakt/saarbruecken/) and Vice-Chair of the Board of Directors, Director of Strategy, [CLAIRE (Confederation of Laboratories for Artificial Intelligence Research in Europe)](https://claire-ai.org/)

## Topics and Convenors

The high-level topics selected for the event and the convenors are:

  - The planet and biodiversity
    - Bonnie Lei, Head of Global Strategy Partnerships - AI for Earth, Microsoft
  - Weather and climate science
    - Peter Dueben, European Centre for Medium Range Weather Forecasts (ECMWF)
  - Natural language processing
    - Dominique Guellec, Scientific Advisor to the Observatory of Science and Technology (OST)
    - Jean-Marc Deltorn, University of Strasbourg and UNISTRA - Centre d'études internationales de la propriété intellectuelle (CEIPI)
  - Health (including COVID & medicine)
    - Anurag Agrawal, Director of the Institute of Genomics and Integrative Biology (CSIR-IGIB)
    - Tiago Cravo Oliveira Hashiguchi, Organisation for Economic Co-operation and Development (OECD)
  - Particle and nuclear physics
    - Jennifer Ngadiuba, Fermi National Accelerator Laboratory (Fermilab)
    - Markus Diefenthaler, Thomas Jefferson National Accelerator Facility (JLab)
  - Astrophysics and astronomy
    - Elena Cuoco, Head of the Data Science Office at the European Gravitational Observatory and Scuola Normale Superiore di Pisa
    - Željko Ivezić, University of Washington
    - Lukas Nellen, Universidad Nacional Autónoma de México (UNAM)
  - Ethics and policy
    - Karine Perset, Organisation for Economic Co-operation and Development (OECD)
    - Alan Paic, Organisation for Economic Co-operation and Development (OECD)
  - Quantum computing
    - Scott Hamilton, Senior Expert of Emerging Technologies at ATOS
    - Eric Aquaronne, IBM Worldwide Systems Strategy and IBM Quantum Ambassador
    - Sofia Vallecorsa, CERN OpenLab
    - Adhvan Novais Furtado, Executive Manager, SENAI CIMATEC
    - Wolfgang Lechner CEO, ParityQC
    - Neil Abroug, National Coordinator of the Quantum strategy, General Secretariat for Investment, Prime Minister’s Office, France
  - AI enabling technologies for daily life
    - Christoffer Petersson, Zenseact
    - Vivien Bonvin, NetGuardians
    - Xavier Perrotton, Driving Assistance Research R&I Software Department Manager and AI Senior Expert, VALEO
    - Nozha Boujemaa, Data & Ethics Manager, Ingka Group, IKEA Group Digital
    - Ivan Meza, Natural Language Processing and Indigenous Languages, IIMAS, UNAM


## Schedule

The daily sessions are planned to take place 7-11 hours Central Daylight Time (14-18 hours Central European Summer Time), with a 15 minute break in the middle. More information in our [timetable](../session).
