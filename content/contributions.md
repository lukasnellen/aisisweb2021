---
title: "Registration and Call for Contributions"
date: 2021-05-08T11:20:53-05:00
draft: false
weight: 25
---
Due to the uncertainties about the possibilities to travel and to hold in-person meetings, the event will be held online.

## Registration

The registration will remain open until the end of the event.

For registration, please use the [registration form][registration] of the [indico system][indico]. The system also provides the [list of registered participants][registered].

## Fees and Access to sessions and material

There will be **no fees required** to participate in the event. Registered participants will receive the link to participate in the online sessions of the event and for access to non-public material.

## Important Dates

- May 20, 2021: Call for contributions opens
- July 11, 2021: Call for contributions closes
- late July, 2021: Decisions announced

## Abstract submission

Please use the [abstract submission form][abstracts] of the [indico system][indico] to register a contribution.

## Proceedings

At this point, the organizers do not plan to publish proceedings of the event.

## Competition

The best contributions in each topic will [receive an award][award].

[award]: {{<  ref "competition.md" >}}
[indico]: https://indico.cern.ch/e/aisis-2021
[registration]: https://indico.cern.ch/event/1041622/registrations/
[registered]: https://indico.cern.ch/event/1041622/registrations/participants
[abstracts]: https://indico.cern.ch/event/1041622/abstracts/
