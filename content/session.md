---
#linkTitle: "Sessions"
#title: "Session layout"
title: "Timetable"
date: 2021-05-17T12:01:36-05:00
draft: false
weight: 20
---
The daily sessions are planned to take place 7-11 hours Central Daylight Time (14-18 hours Central European Summer Time, 12-16 UTC), with a 15 minute break in the middle.

## Block timetable

Hovering over a the time of a session should provide the beginning in local time (browser support required).

<table id="timetable">
<thead>
  <tr>
    <th>
      Time (<span style="color:red;">UTC<span>)
    </th>
    <th>
      October 11
    </th>
    <th>
      October 12
    </th>
    <th>
      October 13
    </th>
    <th>
      October 14
    </th>
    <th>
      October 15
    </th>
  </tr>
</thead>
<tbody>
<tr>
  <td>
  12:00
  </td>
  <td class="special">
    {{< smalltime "2021-10-11T12:00:00" 0 15 >}}</br><a href="{{< relref "sessions/opening" >}}">Introduction</a>
  </td>
  <td class="session" rowspan="8">
    {{< smalltime "2021-10-12T12:00:00" 2 0 >}}</br><a href="../sessions/session3">Session 3 (extended): Health and medicine</a>
  </td>
  <td class="session" rowspan="6">
    {{< smalltime "2021-10-13T12:00:00" 1 30 >}}</br><a href="../sessions/session5">Session 5: AI enabling technologies for daily life</a>
  </td>
  <td class="session" rowspan="6">
    {{< smalltime "2021-10-14T12:00:00" 1 30 >}}</br><a href="../sessions/session7">Session 7: Climate and weather science</a>
  </td>
  <td class="keynote" rowspan="3">
    {{< smalltime "2021-10-15T12:00:00" 0 45 >}}</br><a href="{{< relref "keynotes/abroug" >}}">Keynote 5: Neil Abroug</a>
  </td>
</tr>
<tr>
  <td>
  12:15
  </td>
  <td class="session" rowspan="6">
    {{< smalltime "2021-10-11T12:15:00" 1 30 >}}</br><a href="../sessions/session1">Session 1: Natural language processing</a>
  </td>
</tr>
<tr>
  <td>
  12:30
  </td>
</tr>
<tr>
  <td>
  12:45
  </td>
  <td class="session" rowspan="6">
    {{< smalltime "2021-10-15T12:45:00" 1 30 >}}</br><a href="../sessions/session9">Session 9: Quantum Computing</a>
  </td>
</tr>
<tr>
  <td>
  13:00
  </td>
</tr>
<tr>
  <td>
  13:15
  </td>
</tr>
<tr>
  <td>
  13:30
  </td>
  <td class="keynote" rowspan="3">
    {{< smalltime "2021-10-13T13:30:00" 0 45 >}}</br><a href="../keynotes/stankovich">Keynote 3: Mirjana Stankovich</a>
  </td>
  <td class="keynote" rowspan="3">
    {{< smalltime "2021-10-14T13:30:00" 0 45 >}}</br><a href="../keynotes/bengio">Keynote 4: Yoshua Bengio</a>
  </td>
</tr>
<tr>
  <td>
  13:45
  </td>
  <td class="keynote" rowspan="3">
    {{< smalltime "2021-10-11T13:45:00" 0 45 >}}</br><a href="../keynotes/morvan">Keynote 1: Michel Morvan</a>
  </td>
</tr>
<tr>
  <td>
  14:00
  </td>
  <td class="break">
    BREAK
  </td>
</tr>
<tr>
  <td>
  14:15
  </td>
  <td class="keynote" rowspan="3">
    {{< smalltime "2021-10-12T14:15:00" 0 45 >}}</br><a href="../keynotes/lei">Keynote 2: Bonnie Lei</a>
  </td>
  <td class="break">
    BREAK
  </td>
  <td class="break">
    BREAK
  </td>
  <td class="break">
    BREAK
  </td>
</tr>
<tr>
  <td>
  14:30
  </td>
  <td class="break">
    BREAK
  </td>
  <td class="session" rowspan="6">
    {{< smalltime "2021-10-13T14:30:00" 1 30 >}}</br><a href="../sessions/session6">Session 6: Particle and nuclear physics</a>
  </td>
  <td class="session" rowspan="6">
    {{< smalltime "2021-10-14T14:30:00" 1 30 >}}</br><a href="../sessions/session8">Session 8: Ethics and Policy</a>
  </td>
  <td class="keynote" rowspan="3">
    {{< smalltime "2021-10-15T14:30:00" 0 45 >}}</br><a href="{{< relref "keynotes/slusallek" >}}">Keynote 6: Philipp Slusallek</a>
  </td>
</tr>
<tr>
  <td>
  14:45
  </td>
  <td class="session" rowspan="6">
    {{< smalltime "2021-10-11T14:45:00" 1 30 >}}</br><a href="../sessions/session2">Session 2: Astrophysics and astronomy</a>
  </td>
</tr>
<tr>
  <td>
  15:00
  </td>
  <td class="session" rowspan="4">
    {{< smalltime "2021-10-12T15:00:00" 1 0 >}}</br><a href="../sessions/session4">Session 4: The planet and biodiversity</a>
  </td>
</tr>
<tr>
  <td>
  15:15
  </td>
  <td class="session" rowspan="4">
    {{< smalltime "2021-10-15T15:15:00" 1 0 >}}</br><a href="../sessions/session10">Session 10: Prizes</a>
  </td>
</tr>
<tr>
  <td>
  15:30
  </td>
</tr>
<tr>
  <td>
  15:45
  </td>
</tr>
<tr>
  <td>
  16:00
  </td>
  <td colspan="3">
  </td>
</tr>
<tr>
  <td>
  16:15
  </td>
  <td class="keynote" rowspan="3">
  {{< smalltime "2021-10-12T16:15:00" 0 45 >}}</br>
  <a href="{{< relref "keynotes/villani" >}}">Keynote 7: Cédric Villani</a>
  </td>
  <td colspan="3">
  </td>
  <td class="special">
    {{< smalltime "2021-10-15T16:15:00" 0 15 >}}</br><a href="{{< relref "sessions/closing" >}}">Final Remarks and Outlook: Rafael Mayo</a>
  </td>
</tr>
<tr>
  <td>
  16:30
  </td>
  <td colspan="4">
  </td>
</tr>
<tr>
  <td>
  16:45
  </td>
  <td colspan="4">
  </td>
</tr>


</tbody>
</table>

## Time zone conversions

The starting time of 12:00 UTC corresponds to:

IST (New Delhi)
: 17:30

CEST (Berlin, Paris, Rome, Geneva)
:  14:00

EDT (New York, Washington DC)
:  08:00

CDT (Chicago, Mexico City)
:  07:00

PDT (San Francisco, Seattle)
:  05:00
