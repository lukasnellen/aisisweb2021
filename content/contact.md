---
title: "Contact"
date: 2021-06-03T20:57:07-05:00
draft: false
weight: 200
---
For questions or comments, feel free to contact the organizing committee at

> ai-symposium-2021@correo.nucleares.unam.mx
