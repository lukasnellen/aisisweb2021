---
linkTitle: Keynotes (old)
title: "Keynote speakers"
date: 2021-06-21T00:49:09-05:00
draft: true
weight: 15
---

- Yoshua Bengio, Scientific Director, [MILA](https://mila.quebec/en/)
- Bonnie Lei, Head of Global Strategic Partnerships - [AI for Earth at Microsoft](https://www.microsoft.com/en-us/ai/ai-for-earth)
- Mirjana Stankovich, Senior Digital Policy Specialist, [Center for Digital Acceleration, DAI](https://www.dai.com/)
- Neil Abroug, [National Coordinator of the Quantum strategy](https://www.gouvernement.fr/en/quantum-plan), [General Secretariat for Investment](https://www.gouvernement.fr/le-secretariat-general-pour-l-investissement), Prime Minister’s Office, France
- Michel Morvan, co-founder and Executive Chairman of [Cosmo Tech](https://cosmotech.com/)
- Prof. Dr.-Ing. Philipp Slusallek, Executive Director, [German Research Centre for Artificial Intelligence (DFKI)
-   Saarbrücken](https://www.dfki.de/web/ueber-uns/standorte-kontakt/saarbruecken/) and Vice-Chair of the Board of Directors, Director of Strategy, [CLAIRE (Confederation of Laboratories for Artificial Intelligence Research in Europe)](https://claire-ai.org/)
