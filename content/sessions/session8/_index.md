---
#linkTitle:
title: "Session 8: Ethics and Policy"
date: 2021-09-21T22:56:24-05:00
draft: false
weight: 108
---
Many concerns pertain to governance, ethical and societal issues concerning AI, including transparency, explainability, security, privacy, contestability. The panel will explore these issues as addressed by various national and international instances. For example, the OECD’s Principles on Artificial Intelligence  – the first intergovernmental standard on AI – promote AI that is innovative and trustworthy and that respects human rights and democratic values.

### Time

{{< time "2021-10-14T14:30:00Z" 1 30 "Session 8" >}}

### Panelists

- **Heather Benko**, Senior Manager at American National Standards Institute
- **Marko Grobelnik**,  Chief Technical Officer at IRCAI -- International Research Center on Artificial Intelligence and researcher at AI Lab at Institut Jozef Stefan in Ljubljana, Slovenia
- **Sebastian Hallensleben**, Head of Digitalisation and AI at VDE Verband der Elektrotechnik Elektronik Informationstechnik e.V.
- **Julien Chiaroni**, Director for grand challenges in Artificial Intelligence at the General Secretariat for Investment, France
- **Carolyn Nguyen**, Director, Technology Policy at Microsoft.
- **Juha Heikkila**, Head of the Robotics and Artificial Intelligence unit in the Directorate-General for Communications Networks, Content and Technology, Robotics & Artificial Intelligence, European Commission

### Conveners

- **Karine Perset**, Organisation for Economic Co-operation and Development (OECD)
- **Alan Paic**, Organisation for Economic Co-operation and Development (OECD)
