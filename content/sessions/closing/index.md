---
#linkTitle:
title: "Closing"
date: 2021-10-15T21:45:58-05:00
draft: false
weight: 111
---
### Time

{{< time "2021-10-15T16:15:00" 0 15 "Closing">}}

{{< video src="closing.mp4" >}}

### Conclusions and AISIS 2022

After a very successful event, the organizers would like to thank everybody whom made this event a success. We would like to thank our speakers, conveners, and panelists. We are also grateful to the technical staff for their help during the event.

After this success, we are looking forward to the next edition of the *Symposium on Artificial Intelligence for Science, Industry and Society*, which is planned for late 2022. It will be hosted by [CIEMAT](https://www.ciemat.es/) in Spain.
