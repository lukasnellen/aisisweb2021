---
#linkTitle:
title: "Session 3: Health and Medicine"
date: 2021-09-21T22:56:24-05:00
draft: false
weight: 103
---
Artificial intelligence is expected to play an increasingly important role in health and medicine, with applications ranging from deeper understanding of what constitutes and determines health to improving healthcare systems. Deep integration of data-science and AI with biomedicine is an essential part of new directions such as precision medicine and precision public health. This session will provide an overview of where we are, where we hope to be, and the likely path in-between.

### Time

{{< time "2021-10-12T12:00:00Z" 2 0 "Session 3" >}}

### Panelists

- **Aman Gill** - Project Lead, International Digital Health and Artificial Intelligence Research, The Graduate Institute of International and Development Studies
- **Indra Joshi** - Director of AI for NHSX (digital unit for National Health System, UK)
- **Jessica Morley** - BM Data Policy Lead, Primary Care Health Sciences at University of Oxford
- **Naomi Lee** - Vice Chair at ITU/WHO Focus Group on Artificial Intelligence for Health
- **Ran Balicer** - Chief Innovation Officer at Clalit Health Services & Founding Director

### Conveners

- **Anurag Agrawal**, Director of the Institute of Genomics and Integrative Biology (CSIR-IGIB)
- **Tiago Cravo Oliveira Hashiguchi**, Organisation for Economic Co-operation and Development (OECD)
