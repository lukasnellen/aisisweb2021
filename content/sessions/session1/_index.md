---
#linkTitle: Sessions
title: "Session 1: Natural Language Processing"
date: 2021-08-30T08:00:00-05:00
draft: false
weight: 101
---
With the increased availability of voluminous, digital text collections, the use of natural language processing (NLP) tools to organize and analyze data has become ubiquitous. Recent advances in NLP, largely led by progress in deep learning architectures, have given rise to a new wave of developments, allowing to explore unstructured text corpora at scale, with applications ranging from named entity resolution and document classification, information extraction and semantic relationship analysis, to topic modelling and content clustering, to data generation and prediction.

A promising avenue for NLP applications centers on charting the wealth of scientific and technological information contained in large text collections. Indeed, from scientific publications to patents, from social networks to public datasets, NLP tools can be used to identify novel relationships and unveil patterns in the data that are hidden to other types of investigations. This session aims at presenting recent applications of NLP to *mapping science, industry and society to generate novel ideas, foster scientific discoveries and inventions and capture new scientific and technological trends*.

### Time

{{< time "2021-10-11T12:15:00Z" 1 30 "Session 1">}}

### Panelists

- **Atilla Kaan Alkan**, CEA, IRFU, Université Paris-Saclay
- **Angelo Salatino**, The Open University
- **Daniel Hain**, Aalborg University Business School
- **Lu Zhi Yong**, NCBI/NLM/NIH
- **Kadi Liis Saar**, Research Fellow at St John’s College, University of Cambridge

### Conveners

- **Dominique Guellec**, Scientific Advisor to the Observatory of Science and Technology (OST)
- **Jean-Marc Deltorn**, University of Strasbourg and UNISTRA - Centre d'études internationales de la propriété intellectuelle (CEIPI)
