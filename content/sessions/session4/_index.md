---
#linkTitle:
title: "Session 4: The Planet and Biodiversity"
date: 2021-09-21T22:56:24-05:00
draft: false
weight: 104
---
Artificial intelligence and machine learning approaches hold immense potential when being applied towards monitoring, modeling, and managing our planet's biodiversity. Tune in to this session to hear from researchers at the vanguard of applying AI at cloud scale towards better understanding and conserving species and ecosystems around the globe.

{{< time "2021-10-12T15:00:00Z" 1 0 "Session 4" >}}

### Panelists

- **Naomi Bates**, Future Generations University (US)
- **Arlene Young**, Coastal Zone Management Authority and Initiative (Belize)
- **Ruben Valbuena**, Bangor University (UK)
- **Wesley Welch**, Synthetik Technologies (US)

### Conveners

- **Bonnie Lei**, Head of Global Strategy Partnerships - AI for Earth, Microsoft
