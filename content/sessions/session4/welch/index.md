---
#linkTitle:
title: "Wesley Welch: Subaquatic Classifier for Real-Time Acoustics of Marine Mammals"
date: 2021-09-21T23:38:44-05:00
draft: false
weight: 1
---
{{< video src="aisis_video_synthetik.mp4" >}}

{{% abstract %}}
The detection and identification of marine mammals is of vital importance to both regulatory bodies and commercial aquaculture industries. Knowing the locations and migration patterns of these animals can substantially improve the ability of fishing vessels to avoid them, or alert wildlife experts to animals that have become entangled more rapidly. Current methods of manually tagging marine mammals in order to track their movements by satellite are labor intensive and require some amount of luck to successfully tag the animals. Furthermore, acoustic data currently used to identify and locate marine life is processed in an inefficient manner not conducive to proactive or even reactive responses to the presence of marine life. Current processing of acoustic data involves a subaquatic recording device that is placed and then retrieved at a later date, and often the sound data is sent to third-parties for processing and identification of marine species of interest. Improvements in these processes of subaquatic acoustic detection and localization of marine mammals would have dramatic effects on aquaculture industries and species protection regulations.

To this end, operating as a Microsoft AI for Earth grantee we have formulated a passive approach for analyzing subaquatic acoustic patterns in order to identify marine mammals by taxonomic family and further identify them by genus and species. Our approach utilizes spectrogram feature generation and Recurrent Neural Networks to classify the acoustic patterns of marine mammals in order to determine the species. This model is lightweight enough to fit on microcomputers, and has highly accurate performance of 91% at the species level and 97% at the taxonomic family level. Additionally, we offer a supplemental multilateration approach to sound source localization which can accurately determine the 3D coordinates of the marine mammal relative to the hydrophone network. As such, our model can be used to monitor the locations and frequencies of marine mammal activity for use by both industries and regulatory agencies in studying the impact of aquaculture projects on the behaviors of marine mammals. Our hope is to use the data from these systems for both real-time intervention in the interest of species protection and to develop an understanding to better predict future marine mammal behavior.
{{%/ abstract %}}
