---
#linkTitle:
title: "Jeff Adie: Using AI to improve climate and weather modelling"
date: 2021-09-22T00:15:07-05:00
draft: false
weight: 1
---
{{< video src="jeffadie_ai_for_cwo.mp4" >}}

{{% abstract %}}
Climate and Weather modelling is a computationally intensive process even on the fastest supercomputers in the world. Modern AI methods are increasingly being applied to these problems to accelerate the time to solution and to reduce the computational requirements. This talk introduces some of these methods and then details some areas of active research within our group to improve future modelling and simulation efforts.
{{%/ abstract %}}
