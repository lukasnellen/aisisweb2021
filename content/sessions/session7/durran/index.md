---
#linkTitle:
title: "Dale Durran: Can deep learning replace current numerical weather prediction models?"
date: 2021-09-22T00:14:59-05:00
draft: false
weight: 1
---
{{< video src="Durran_Weyn_NWP.mp4" >}}

[Slides](DLWP_AISIS_2021.pdf)

{{% abstract %}}
We present a data-driven global weather-forecasting framework, named deep learning weather prediction (DLWP), which uses a deep convolutional neural network (CNN) to forecast six key atmospheric variables. In addition to the forecast fields, three external fields are specified in the model’s inputs: a land-sea mask, topographic height, and top-of-atmosphere incoming solar radiation. The model is recursively stepped forward in 12-hour time steps while representing the atmospheric fields with 6-hour temporal and roughly 1.4 x 1.4 degree spatial resolution. In order to minimize distortions from computing CNN stencils on the sphere, our model operates on a cubed-sphere grid.
The extreme computational efficiency of our DLWP model allows us to create an ensemble prediction system requiring just three minutes on a single GPU to produce a 320-member set of six-week forecasts. These forecasts are all equally-likely realizations of future weather, with differences between them primarily produced by randomizing the training process to create a set of 32 CNNs with slightly different CNN learned weights.
Our model is capable of producing a reasonable 4.5-day deterministic forecast of Hurricane Irma and it spontaneously generates mid-latitude and tropical cyclones in a one-year free-running simulation. An offline diagnostic model is also trained to derive precipitation from other six forecast fields. Averaged globally and over a two-year test set, the ensemble mean RMSE retains skill relative to climatology beyond two-weeks, with anomaly correlation coefficients remaining above 0.6 through six days.
Our primary application is to subseasonal-to-seasonal (S2S) forecasting at lead times from two to six weeks. Current forecast systems have low skill in predicting one- or 2-week-average weather patterns at S2S time scales and require computationally-demanding large ensembles to make probabilistic forecasts. Probabilistic skill scores show that our system performs similarly to the European Centre for Medium Range Weather Forecasts (ECMWF) S2S ensemble at lead times of 4 and 5-6 weeks. At shorter lead times, the much more computationally demanding ECMWF ensemble performs better than DLWP.
{{%/ abstract %}}
