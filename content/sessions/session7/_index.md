---
#linkTitle:
title: "Session 7: Climate and Weather Science"
date: 2021-09-21T22:56:24-05:00
draft: false
weight: 107
---
Artificial intelligence and machine learning is used for weather and climate science in various ways for many different applications. However, while the Earth science community has learned to walk, regarding the use of machine learning, there are a couple of domain specific challenges that still prevent us from running. The session will present progress, name challenges, and discuss potential solutions.

### Time

{{< time "2021-10-14T12:00:00Z" 1 30 "Session 7" >}}

### Panelists

- **Alvin Chew**, Bentley Systems, Inc.
- **Bing Gong**, Jülich Supercomputing Centre, Forschungszentrum Jülich, 52425 Jülich, Germany
- **Jeff Adie**, NVIDIA AI Technology Centre, Singapore
- **Dale R. Durran**, University of Washington
- **Thomas Chen**, Academy for Mathematics, Science, and Engineering

### Conveners

- **Peter Dueben**, European Centre for Medium Range Weather Forecasts (ECMWF)
