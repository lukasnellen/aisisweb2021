---
#linkTitle:
title: "Dispersive Shallow Water Modelling via Multiscale Neural Networks with UPC"
speaker: "Alvin Chew"
date: 2021-10-13T21:16:15-05:00
draft: False
weight: 1
---
{{< video src="Final slides recording_mpinn-upc.mp4" >}}

{{% abstract %}}
This study develops a generic engineering modelling approach, termed as multiscale physics induced neural network implemented with unified parallel C (MPINN-UPC), which comprises of a series of systematic analyses to model the fluid behavior in shallow waters for flood modelling in urbanized cities. The proposed analyses consist of 2 key phases, namely: (Phase 1) derive homogenized equations, via homogenization theory coupled with multiscale perturbation analysis, for effective features selection and developing useful activation functions for infusion into selected hidden layers of the MPINN model(s); and (Phase 2) UPC implementation of MPINN model(s) to improve its computational performance for model training and validation. Using historical field data for modelling and forecasting peak streamflow heights, the resulting prediction and computational performance results are encouraging, hence providing the possibility of extending the proposed approach to other fluid mechanics applications. Results comparison with that of other traditional machine learning (ML) models is also performed to underline the clear advantage provided by the proposed MPINN-UPC approach in its predictive capability to forecast peak streamflow heights for flood modelling.
{{% /abstract %}}
