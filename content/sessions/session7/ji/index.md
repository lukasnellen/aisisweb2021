---
#linkTitle:
title: "Yan Ji: Deep Learning for Weather Forecasts"
date: 2021-09-22T00:15:11-05:00
draft: false
weight: 1
---
{{< video src="Yan_AISIS_DeepLearningForWeatherForecasts.mp4" >}}

{{% abstract %}}
Accurate weather predictions are highly demanded by society. This study explores the adaptation of state-of-the-art deep learning architectures for video frame prediction in the context of weather applications. Proof-of-concept case studies are performed to 2m temperature forecasts up to 12 hours over central Europe, and precipitation nowcasting up to 2 hours over south China. The pixel-wise loss-based convolutional Long Short Term Memory architectures (ConvLSTM) and GAN’s variant architecture, stochastic adversarial video prediction (SAVP), are used and compared with standard persistent forecasts for 2m temperature, and traditional optical flow method for precipitation, respectively. Mean square error (MSE), anomaly correlation coefficient (ACC), and Structural Similarity Index (SSIM) are utilized to evaluate the 2m temperature forecast. The method of object-based diagnostic evaluation (MODE) was particularly adopted for precipitation nowcasting to evaluate the attributions of rain events in terms of centroid, intensity, and shape. Finally, the sensitivity was performed to test the models' robustness to input variables, target regions, and the number of training samples.
{{%/ abstract %}}
