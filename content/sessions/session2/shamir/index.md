---
#linkTitle:
title: "Lior Shamir: Artificial intelligence challenges in analysis of large astronomical databases"
date: 2021-09-21T23:32:27-05:00
draft: false
weight: 1
---
{{< video src="AISIS_2021_lior_shamir.mp4" >}}

{{% abstract %}}
Modern astronomy has been becoming increasingly more dependent on digital sky surveys powered by robotic telescopes, and that trend is bound to continue. These powerful imaging systems are continuously monitoring the sky, generating some of the world’s largest and most complex databases. These databases are far too large and too complex for manual analysis, reinforcing the need of artificial intelligence methods that can turn these large and complex databases into knowledge to optimize their scientific return. Here I discuss some of the basic artificial intelligence and machine learning tasks that are required for making scientific discoveries in large astronomical databases. For instance, the common machine learning task of automatic image classification is a common task used in astronomy to turn databases of billions of objects into structured catalogs. Query-by-example can be used to identify a collection of objects of interest in very large heterogenous data, a task that would be highly impractical to perform manually given the typical size of astronomical databases. Identification of changes in a large number of monitored astronomical objects can be used to identify variability, transients, minor planets, exoplanets, and more. Automatic novelty and outlier detection can be used to identify rare peculiar objects of paramount scientific interest that might be hidden among millions of “regular” objects. These tasks are critical in the advancement of modern astronomy in the information era. Practical application of these methods to real-world astronomical databases led to different discoveries made in telescopes such as the Sloan Digital Survey Telescope, the Panoramic Survey Telescope and Rapid Response System, Hubble Space Telescope, and DESI Legacy Survey. These discoveries include objects of interest such as gravitational lenses, peculiar galaxies, analysis of structured astronomical data, and identification of rare objects that would have been extremely difficult to identify without using automation.
{{%/ abstract %}}
