---
#linkTitle: Sessions
title: "Session 2: Astrophysics and Astronomy"
date: 2021-08-30T08:00:00-05:00
draft: false
weight: 102
---
The growing size of modern astronomical surveys makes a reliable analysis using established image processing techniques difficult. AI based analysis promises to provide a viable solution to handing the ever increasing data volumes. Other areas of astronomy and astrophysics also benefit significantly from deep neural networks for classification purposes and to generate simulated data.

{{< time "2021-10-11T14:45:00Z" 1 30 "Session 2" >}}

### Panelists

- **Francisco Forster** - Adjunct Professor at Center for Mathematical Modeling Associate Researcher at Millennium Institute for Astrophysics
- **Marco Cavaglià** - Professor of Physics at the Missouri University of Science and Technology
- **Ting-Yun Cheng** - Postdoctoral Research Associate at Durham University
- **Yuan-Sen Ting** - Australian National University

### Conveners

- **Elena Cuoco**, Head of the Data Science Office at the European Gravitational Observatory and Scuola Normale Superiore di Pisa
- **Željko Ivezić**, University of Washington
- **Lukas Nellen**, Universidad Nacional Autónoma de México (UNAM)
