---
#linkTitle:
title: "Biprateep Dey: Interpretable Photometric Redshifts using Deep Capsule Networks"
date: 2021-09-22T01:05:47-05:00
draft: false
weight: 1
---
{{< video src="AISIS_Presentation_Dey.mp4" >}}

{{% abstract %}}
Studies of cosmology, galaxy evolution, and astronomical transients with the next generation of imaging surveys (like LSST) are all critically dependent on estimates of galaxy redshifts from imaging data alone. Capsule networks are a new type of neural network architecture that is better suited for identifying morphological features of the input images than traditional Convolutional Neural Networks. We use a Deep Capsule Network trained on about 0.5 million galaxy images and their spectroscopic redshifts from the Sloan Digital Sky Survey (SDSS) along with their morphologies from the Galaxy Zoo project to jointly predict their morphologies and redshifts of galaxies from their images. We obtain near-human level morphology classification accuracies and obtain photometric redshift prediction accuracy comparable or better than the current state-of-the-art methods while requiring less data. We also obtain a low dimensional encoding of the galaxy images which we use to investigate the decision making process of the neural network. We show that the neural network has learnt physical properties of the galaxies like magnitudes, colours, size, etc. We also use the low dimensional encoding to predict various properties of galaxies using gradient boosted decision trees and achieve competitive results.
{{%/ abstract %}}
