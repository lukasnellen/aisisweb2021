---
#linkTitle:
title: "On Modelling Complex Systems in Astronomy"
speaker: "Yuan-Sen Ting"
date: 2021-10-08T09:53:37-05:00
draft: false
weight: 1
---
{{< video src="yan-sen-ting-aisis-2021.mp4" >}}

{{% abstract %}}
Astronomy today is fundamentally different than it was even just a decade ago. Our increasing ability to collect a large amount of data from ever more powerful instrumental has enabled many new opportunities. However, such opportunity also comes with new challenges. Fundamentally, the bottleneck stems from the fact most astronomical observations are inherently high dimension — from “imaging” the Universe at the finest details to fully characterizing tens of millions of spectra which consists of tens of thousands of wavelengths. In this regime, the classical astrostatistics approaches might struggle.

To this end, I have developed and will present two different machine learning approaches to quantify complex systems in astronomy. (1) Reductionist approach: I will discuss how machine learning can inspire better summary statistics, such as scattering transform, which optimally compress information and extract higher-order moment information in stochastic processes. (2) A generative approach. I will discuss how generative models, such as normalizing flow, allows us to properly model the vast astronomy data set, enabling the study of complex astronomy systems directly in their raw dimension
{{% /abstract %}}
