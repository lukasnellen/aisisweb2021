---
#linkTitle:
title: "Can deep learning replace current numerical weather prediction models?"
speaker: "Dale Durran (Society)"
date: 2021-10-15T23:14:13-05:00
draft: false
weight: 30
---
{{< video src="prize-durran.mp4" >}}
