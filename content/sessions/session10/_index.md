---
#linkTitle:
title: "Session 10: Prizes"
date: 2021-09-21T22:56:24-05:00
draft: false
weight: 110
---
The [AISIS prizes](../../competition/) are aimed at recognizing the most innovative and/or impactful contributions submitted to the symposium, with an emphasis on those which are cross-disciplinary or otherwise relevant beyond a specific topic of interest. There are three prizes, as judged by the full organization committee. This session provided prize recipients with an opportunity to give a plenary talk to convey their ideas to the symposium.

### Time

{{< time "2021-01-15T15:15:00Z" 1 0 "+session+10" >}}

### Winners

AISIS 2021 prize for an outstanding contribution of relevance to...

- **Science** *Atilla Kaan Alkan*: NLP for analyzing messages of Astrophysical observations
- **Industry** *Eric Aquaronne*: IBM Quantum: An Introduction
- **Society** *Dale Durran*: Can deep learning replace current numerical weather prediction models?

{{< video src="prize-presentation.mp4" >}}

[slides](AISIS-prizes.pdf)
