---
#linkTitle:
title: "Session 5: AI Enabling Technologies for Daily Life"
date: 2021-09-21T22:56:24-05:00
draft: false
weight: 105
---
Applications of AI have left the laboratories and are becoming a mainstay of our interaction with the digital world. Our new digital companions enable us to perform new activities, but their use also creates new risks.

### Time

{{< time "2021-10-13T12:00:00Z" 1 30 "Session 4" >}}

### Panelists

- **Christoffer Petersson**, Zenseact
- **Vivien Bonvin**, NetGuardians
- **Xavier Perrotton**, Driving Assistance Research R&I Software Department Manager and AI Senior Expert, VALEO
- **Nozha Boujemaa**, Global Data & AI Ethics Manager, IKEA Retail (Ingka Group)
- **Ivan Meza**, Natural Language Processing and Indigenous Languages, IIMAS, UNAM

### Moderator

- **Lukas Nellen**, Universidad Nacional Autónoma de México (UNAM)
