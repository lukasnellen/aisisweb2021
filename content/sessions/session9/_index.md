---
#linkTitle:
title: "Session 9: Quantum Computing"
date: 2021-09-21T22:56:24-05:00
draft: false
weight: 109
---
Quantum computing is the next frontier in high-performance computing. Using qbits instead of bits means that values aren’t zero or one, they can take any value between zero and one, meaning that the algorithms can be made massively parallel. It thus means that quantum computing can take machine learning a step closer to human learning. Significant challenges remain to be resolved until quantum computing can be used as a mainstream computing tool, which require heavy investment and can potentially aggravate the digital divide between North and South.

### Time

{{< time "2021-10-15T12:45:00Z" 1 30 "Session 9">}}

### Panelists

- **Scott Hamilton**, Senior Expert of Emerging Technologies at ATOS
- **Eric Aquaronne**, IBM Worldwide Systems Strategy and IBM Quantum Ambassador
- **Sofia Vallecorsa**, CERN OpenLab
- **Adhvan Novais Furtado**, Executive Manager, SENAI CIMATEC
- **Wolfgang Lechner**, CEO, ParityQC
- **Neil Abroug**, National Coordinator of the Quantum strategy, General Secretariat for Investment, Prime Minister’s Office, France

### Moderator

- **Alan Paic**, Senior Policy Analyst, Science and Technology Policies, OECD

### Additional information

A link with additional material will be sent to the [registered participants](../../contributions) of the symposium. 
