---
#linkTitle:
title: "Session 6: Particle and Nuclear Physics"
date: 2021-09-21T22:56:24-05:00
draft: false
weight: 106
---
AI in High Energy Physics (HEP) and Nuclear Physics (NP) represents the next generation of methods to build models from data and to use these models alone or in conjunction with simulation and scalable computing to advance research in fundamental physics. These methods include (but are not limited to) machine learning (ML) and deep learning (DL). ML techniques have a long history in HEP. With the advent of modern DL networks, their use expanded widely and is now ubiquitous to both HEP and NP, as found promising for many different purposes like anomaly detection, event classification, simulations, or the design and operation of large-scale accelerator facilities and experiments.

### Time

{{< time "2021-10-13T14:30:00Z" 1 30 "Session 6" >}}

### Panelists

- **Cristiano Fanelli**, Nuclear physics (experiment) ,MIT
- **Fernanda Psihas**, HEP (experiment), FNAL
- **Gregor Kasieczka**, HEP (experiment), Hamburg
- **Tanja Horn**, Nuclear physics (experiment), CUA

### Conveners

- **Jennifer Ngadiuba**, Fermi National Accelerator Laboratory (Fermilab)
- **Markus Diefenthaler**, Thomas Jefferson National Accelerator Facility (JLab)
