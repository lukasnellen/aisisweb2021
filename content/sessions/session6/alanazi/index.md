---
#linkTitle:
title: "Yasir Alanazi: Simulation of Electron-Proton Scattering Events by a Feature-Augmented and Transformed Generative Adversarial Network (FAT-GAN)"
date: 2021-09-21T23:49:34-05:00
draft: false
weight: 1
---
{{< video src="FAT-GAN-Yasir.mp4" >}}

{{% abstract %}}
We present a Machine Learning Event Generator (MLEG) based on Generative Adversarial Networks (GANs) to mimic events at their final state. Our model selects a set of transformed features from particle momenta that can be generated easily by the generator, and uses these to produce a set of augmented features that improve the sensitivity of the discriminator. The new Feature Augmented and Transformed GAN (FAT-GAN) is able to faithfully reproduce the distribution of final state electron momenta in inclusive electron scattering, without the need for input derived from domain-based theoretical assumptions.
{{%/ abstract %}}
