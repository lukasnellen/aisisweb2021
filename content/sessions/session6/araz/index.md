---
#linkTitle:
title: "Jack Araz: Quantum-inspired event reconstruction with Tensor Networks"
date: 2021-09-21T23:48:49-05:00
draft: false
weight: 1
---
{{< video src="jackaraz.mp4" >}}

[slides](aisis_jackaraz.pdf)

{{% abstract %}}
Tensor Networks are non-trivial representations of high-dimensional tensors, originally designed to describe quantum many-body systems. In this talk, we will show that Tensor Networks are ideal vehicles to connect quantum mechanical concepts to machine learning techniques, thereby facilitating an improved interpretability of neural networks. This study presents the discrimination of top quark signal over QCD background processes using a Matrix Product State classifier. We show that entanglement entropy can be used to interpret what a network learns, which can be used to reduce the complexity of the network and feature space without loss of generality or performance. For the optimisation of the network, we compare the Density Matrix Renormalization Group (DMRG) algorithm to stochastic gradient descent (SGD) and propose a joined training algorithm to harness the explainability of DMRG with the efficiency of SGD.
{{%/ abstract %}}
