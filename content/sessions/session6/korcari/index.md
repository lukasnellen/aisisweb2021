---
#linkTitle:
title: "William Korcari: Shared Data and Algorithms for Deep Learning in Fundamental Physics"
date: 2021-09-21T23:49:25-05:00
draft: false
weight: 1
---
{{< video src="AISIS_WK_talk.mp4" >}}

{{% abstract %}}
We introduce a collection of datasets from fundamental physics research including particle physics, astroparticle physics, hadron, and nuclear physics for supervised machine learning studies. These datasets, containing hadronic top quarks, cosmic air showers, phase transitions in the hadronic matter, and generator-level histories, are combined and made public to simplify future work on cross-disciplinary machine learning and transfer learning in fundamental physics. Based on these samples, we present two simple and yet flexible models: a fully connected neural network and a graph-based neural network architecture that can easily be applied to a wide range of supervised learning tasks in these domains. Furthermore, we show that our approaches reach performance close to state-of-the-art dedicated methods on all datasets.
{{%/ abstract %}}
