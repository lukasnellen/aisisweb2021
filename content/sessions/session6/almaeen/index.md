---
#linkTitle:
title: "Manal Almaeen: Variational Autoencoder Inverse Mapper: An End-to-End Deep Learning Framework for Inverse Problems"
date: 2021-09-21T23:49:15-05:00
draft: false
weight: 1
---
{{< video src="VAIM_AISISI_Almaeen.mp4" >}}

{{% abstract %}}
We present a new Machine Learning technique based on Variational Autoencoders in the context of deep learning to construct an effective “inverse function” that maps experimental data into quantum correlation functions (QCFs) such as parton distribution functions in the nucleon. As such it provides a powerful complementary tool for QCD global analysis where the Bayesian inference associate with the inverse problem of QCFs can be implemented efficiently allowing the possibility to explore systematically different choices for the likelihood functions, Bayesian priors and have the possibility to understand in great detail how each data point or sets of data points influence the uncertainty quantification for the QCFs.

(Manal Almaeen is supported by a graduate fellowship from Center for Nuclear Femtography, SURA, Washington DC)
{{%/ abstract %}}
