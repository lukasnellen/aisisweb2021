---
#linkTitle:
title: "Online Multiscale Method for Change Detection in
Automated Data-Quality Monitoring"
speaker: "Ronglong Fang"
date: 2021-10-05T17:00:04-05:00
draft: False
weight: 1
---
{{< video src="Ronglong Fang_Online_DQM-aisis-2021.mp4" >}}

[Slides](Ronglong%20Fang_Online_Multiscale_Method_for_Change_Detection_in_Automated_Data_Quality_Monitoring.pdf)

{{% abstract %}}
Automated data-quality monitor is import in both theory and application. Finding anomalies in the data, e.g., from background to the measurements or problems with the detectors, will determine the quality of data analysis results. We develop an online multiscale method to detect the change for the steaming data. This method is motivated by on the multiscale representation of functions. We use the real physics data to test our algorithm, the results shows that our algorithm is much more efficient. Thus, our is more useful for the large dataset.
{{% /abstract %}}
