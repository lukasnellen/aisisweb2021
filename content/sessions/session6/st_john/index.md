---
#linkTitle:
title: "Jason St. John: On-Chip ML Control Agent for Precision Regulation at the Fermilab Booster Synchrotron"
date: 2021-09-21T23:49:00-05:00
draft: false
weight: 1
---
{{< video src="JasonStJohn-OnChipMLControlAgentforPrecisionRegulationattheFermilabBoosterSynchrotron_AISIS2021.mp4" >}}

{{% abstract %}}
Fifteen times per second the Fermilab Booster synchrotron receives a 400 MeV proton beam, accelerates it to 8000 MeV for use by diverse particle accelerators or experiments, and resets to begin again. High precision regulation of the Gradient Magnet Power Supply (GMPS) keeps the circulating beam orbiting through the optimal trajectory and maximizes throughput efficiency, which has direct, positive consequences on the amount of science the lab accomplishes. Regulation is made necessary by unpredictable variations in delivered power and by the programmed operation of other large, high-current electrical loads nearby. We report on superseding the existing PID regulation circuit, first developing a surrogate model of the control environment, then using Reinforcement Learning (RL) to train an agent to predictively compensate the GMPS control signal. The agent is deployed on an FPGA for fast, reliable forward inference. The approach used here has wide applicability to real-time control problems in the Fermilab accelerator complex and beyond.
{{%/ abstract %}}
