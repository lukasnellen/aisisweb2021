---
#linkTitle:
title: "Felix Wagner: A Python Package with Novel Raw Data Analysis Methods for Cryogenic Particle Detectors"
date: 2021-09-21T23:48:41-05:00
draft: false
weight: 1
---
{{< video src="Cait.mp4" >}}

{{% abstract %}}
Novel cryogenic scintillating calorimeters achieve sub-keV recoil energy thresholds. Such low thresholds require a sensible raw data analysis of triggered events, to identify different types of particle recoils and artifacts despite the low signal-to-noise ratio, and reconstruct the corresponding recoil energy. We present for this purpose the Python package cait (Cryogenic Artificial Intelligence Tools), which utilizes new methods from data science and machine learning.

The implemented features include an interface for the user-friendly labeling of data for supervised models, a range of event simulation tools for data augmentation, tailored data sets and data modules for PyTorch and PyTorch lightning, as well as standard methods for fits, plots, triggering and the energy calibration.

cait is developed open source and available on the Python Package Index. Although initially meant for efficient prototyping of new analysis methods, its latest v1.0 release renders it a fast and production-ready package for the whole analysis process. The package is currently tailored to the needs of the CRESST and COSINUS dark matter searches and is used within both collaborations. Extensions for experiments with similar, time series-like, data formats are possible.
{{%/ abstract %}}
