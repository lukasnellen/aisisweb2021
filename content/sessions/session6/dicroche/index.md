---
#linkTitle:
title: "Machine Learning at CMS"
speaker: "Davide Di Croce"
date: 2021-09-28T23:13:43-05:00
draft: false
weight: 1
---
{{< video src="MLinCMS-DavideDiCroce.mp4" >}}

{{% abstract %}}
The Compact Muon Solenoid (CMS) experiment is one of the two large, general purpose detectors at the European Organization of Nuclear Research (CERN), Large Hadron Collider (LHC). The data are collected from the approximately 130 million detector channels at a rate of 2 MByte at a rate of 40 MHz. Using this data, the properties of the recently discovered Higgs boson and many other physics processes at the very highest particle energies are studied. Managing and analyzing the tens of PetaBytes of data and metadata is a major challenge for the CMS Collaboration. In order to cope with these challenges, the CMS collaboration has relied on state-of-art machine learning algorithms to modernize both the daily operations of the CMS simulation and data processing chain, and to greatly improve the data analysis and physics results. Some examples are the use of the recurrent LSTM architectures for classification tasks and the deep auto encoders for data quality monitoring. Moreover, a variety of BDT and Deep Neural Network based algorithms have been developed in order to optimize the identification and reconstruction of physics objects like electrons, photons, and jets. Many marquee results reported by the CMS Collaboration, like the measurement of the properties of recently discovered Higgs bosons, exploit the use of dedicated machine learning algorithms.
{{%/ abstract %}}
