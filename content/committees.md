---
title: "Committees"
date: 2021-05-07T21:19:16-05:00
draft: false
weight: 40
---
## Scientific Committee

- Benítez, Héctor; DGTIC-UNAM
- Carreón, Pilar; ICN-UNAM
- Escalante Ramírez, Boris; CViCom-UNAM
- Ivezic, Zeljko; University of Washington
- Lee, William; IA-UNAM
- Mayo, Rafael; CIEMAT
- Nellen, Lukas; ICN-UNAM / Co-chair
- Paic, Alan; OECD / Co-chair
- Paic, Guy; ICN-UNAM / Co-chair
- Schramm, Steven; University of Geneva / Co-chair


## Local Organizing Committee

- Díaz, Luciano; ICN-UNAM
- Nellen, Lukas; ICN-UNAM
- Ortiz, Antonio; ICN-UNAM
- Paic, Guy; ICN-UNAM
- Romo, Fabián; DGTIC-UNAM
