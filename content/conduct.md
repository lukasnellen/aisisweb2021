---
title: "Code of conduct for all assistants"
linkTitle: "Code of Conduct"
date: 2021-05-07T21:42:58-05:00
draft: false
weight: 100
---
The AISIS is a community event intended for presentations, networking and collaboration as well as learning. We value a civil and respectful environment which encourages the free expression and exchange of scientific ideas.

Should a lapse of professional decorum occur (such as discrimination or harassment), attendees are encouraged to bring issues, in a confidential setting, to the advisors appointed by the conference organizers. The advisors will suggest ways of redressing the matter and counsel the parties involved. The conference organizers may, after due consideration, take action as they deem appropriate, including, in severe cases, expulsion from the conference.

<!--without refund.-->
