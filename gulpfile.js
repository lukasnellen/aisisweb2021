const { src, dest, series, parallel, watch, task } = require('gulp');
var gulpCopy = require('gulp-copy');
var sass = require('gulp-dart-sass');
const autoprefixer = require('gulp-autoprefixer');


/********* javascript source and destination **********/
js_src = [
  'node_modules/foundation-sites/dist/js/foundation.js',
  'node_modules/foundation-sites/dist/js/foundation.min.js',
  //'node_modules/foundation-sites/dist/js/plugins/*',
  'node_modules/jquery/dist/jquery.js',
  'node_modules/jquery/dist/jquery.min.js',
  'node_modules/what-input/dist/what-input.js',
  'node_modules/what-input/dist/what-input.min.js',
];
js_dst = 'static/js/';


/********* scss / css source and destination **********/
css_src = [
  'src/scss/**/*.scss',
];
css_dst = 'static/css/';


/********* tasks **********/

// Copy javascript files to static
function copy_js() {
    return src(js_src)
      .pipe(gulpCopy(js_dst, {prefix: 4}));
};
task('js', copy_js);

// Compile css
function css() {
  return src(css_src)
    .pipe(sass({includePaths: ['node_modules/foundation-sites/scss']}).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(dest(css_dst));
};
task(css);
// watch changes to scss and run pipline
task('watch', function () { watch(css_src, css); });

// default: copy js and compile css
exports.default = parallel(copy_js, css);
