AISIS 2021 web page
===================

This is the source code for the web page at https://aisis-2021.nucleares.unam.mx/.

The page is static, generated using [hugo].

## Developing contents

To add a page, use the command
```sh
hugo new pages/page-name.md
```
The font-matter determines the title using the `title` and `pre-title` variables. The link in the buttons on the left is taken from the `linkTitle` or `title` variable, in that order of precedence. The buttons can be reorderd using the `weight` variable. Smaller weights appear higher up in the list.

**NB**: To get a new page displayed, set the `draft` variable to `false`.

For development, run
```sh
hugo server
```
to get [hugo] to serve the site and to re-build the site after updates.

## Deploying the site

To update the production server, you have to run
```sh
./deploy.sh  # update server
```
This script will build the site with the default options and synchronize to the server. You need access to the server by registering an ssh key.

### Restricted access using [rrsync]

As part of the contributed tools, [rsync] includes the [rrsync] script to restrict access of a given key to only selected directories. On our server, the static site is at `/data/volumes/aisis-2021` and [rrysnc] is installed as `/usr/local/bin/rrsync`. This means the key in `/.ssh/authorized_keys` should be prefixed by
```
command="/usr/local/bin/rrsync /data/volumes/aisis-2021"
```

[hugo]: https://gohugo.io
[rsync]: https://en.wikipedia.org/wiki/Rsync
[rrsync]: https://git.samba.org/rsync.git/?p=rsync.git;a=blob;f=support/rrsync
