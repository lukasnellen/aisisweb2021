#! /bin/sh

src=$(dirname $0)/public/
dst=auger.nucleares.unam.mx:/data/volumes/aisis-2021/
flags="-Pav --delete-after"

hugo
time rsync ${flags} ${src} ${dst}
